package de.mohamed.amara.repository;

import org.springframework.data.repository.CrudRepository;

import de.mohamed.amara.entity.Student;

public interface StudentenRepository extends CrudRepository<Student, String>{
	Student findByStudentUsername(String username);
}
