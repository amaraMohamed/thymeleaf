package de.mohamed.amara.repository;

import org.springframework.data.repository.CrudRepository;

import de.mohamed.amara.entity.Studiengang;

public interface StudiengangRepository extends CrudRepository<Studiengang, String> {

}
