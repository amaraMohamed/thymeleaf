package de.mohamed.amara.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import de.mohamed.amara.entity.Student;
import de.mohamed.amara.general.model.LoginModel;
import de.mohamed.amara.mapping.MappingsService;
import de.mohamed.amara.repository.StudentenRepository;

@Service("whsSecurityService")
public class WhsStudentenUserDetailsService implements UserDetailsService {

	@Autowired
	StudentenRepository mStudentenRepository;
	
	@Autowired
	MappingsService mMappingsService;
	
	@Override
	public LoginModel loadUserByUsername(String username) throws UsernameNotFoundException {
		Student lStudent = mStudentenRepository.findByStudentUsername(username);
		if (lStudent != null)
			return mMappingsService.StudentToLoginModel(lStudent);
		else
			throw new UsernameNotFoundException("Es gibt keinen User mit dem genannten Username");
		
	}

}
