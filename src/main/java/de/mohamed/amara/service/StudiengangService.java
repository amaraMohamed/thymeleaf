package de.mohamed.amara.service;

import org.springframework.stereotype.Service;

import de.mohamed.amara.entity.Studiengang;
import de.mohamed.amara.general.model.FeStudiengang;
@Service
public class StudiengangService {
	
	public FeStudiengang mapStudiengang(Studiengang pStudiengang) {
		return FeStudiengang.builder().name(pStudiengang.getStudiengangName())
				.uuid(pStudiengang.getStudiengangUuid()).verantwortlicher(pStudiengang.getStudiengangVerantwortlicher())
				.build();
	}
	
}
