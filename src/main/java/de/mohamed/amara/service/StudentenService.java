package de.mohamed.amara.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import de.mohamed.amara.entity.Student;
import de.mohamed.amara.general.model.FeStudent;
import de.mohamed.amara.repository.StudentenRepository;

@Service
public class StudentenService {

	@Autowired
	StudentenRepository mStudentenRepository;

	@Autowired
	StudiengangService mStudiengangService;

	public List<FeStudent> getAll() {
		return mapStudents(Lists.newArrayList(mStudentenRepository.findAll()));
	}

	public FeStudent get(String pFeStudentUuid) {
		return mapStudent(mStudentenRepository.findOne(pFeStudentUuid));
	}

	public ResponseEntity<FeStudent> save(FeStudent pFeStudent) {
		FeStudent lFeStudent = mapStudent(mStudentenRepository.save(mapFeStudent(pFeStudent)));
		return new ResponseEntity<FeStudent>(lFeStudent, HttpStatus.CREATED);
	}

	@SuppressWarnings("rawtypes")
	public ResponseEntity delete(String pFeStudentUuid) {
		mStudentenRepository.delete(pFeStudentUuid);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private Student mapFeStudent(FeStudent pFeStudent) {
		return Student.builder().studentName(pFeStudent.getName()).studentVorname(pFeStudent.getVorname())
				.studentEmail(pFeStudent.getEmail()).studentGeburtsdatum(pFeStudent.getGeburtsdatum())
				.studentMatrikelNr(pFeStudent.getMatrikelNr()).studentUuid(pFeStudent.getUuid()).build();
	}

	private FeStudent mapStudent(Student pStudent) {
		return FeStudent.builder().name(pStudent.getStudentName()).vorname(pStudent.getStudentVorname())
				.geburtsdatum(pStudent.getStudentGeburtsdatum()).email(pStudent.getStudentEmail())
				.matrikelNr(pStudent.getStudentMatrikelNr())
				.studiengang(pStudent.getStudentStudiengang() == null ? null
						: mStudiengangService.mapStudiengang(pStudent.getStudentStudiengang()))
				.uuid(pStudent.getStudentUuid()).build();
	}

	private List<FeStudent> mapStudents(List<Student> pStudents) {
		List<FeStudent> lFeStudents = new ArrayList<>();
		for (Student student : pStudents) {
			lFeStudents.add(mapStudent(student));
		}
		return lFeStudents;
	}

}
