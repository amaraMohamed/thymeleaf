package de.mohamed.amara;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import de.mohamed.amara.entity.Student;
import de.mohamed.amara.repository.StudentenRepository;

@SpringBootApplication
@EnableEurekaClient
public class StudentenServiceApplication {
	
	@Autowired
	StudentenRepository mStudentenRepository;
	
	@PostConstruct
	public void mockDaten() {
		mStudentenRepository.save(Student.builder()
		.isAccountNonExpired(true)
		.isAccountNonLocked(true)
		.isCredentialsNonExpired(true)
		.studentUsername("adesso")
		.studentPassword("adesso")
		.build());
	}

	public static void main(String[] args) {
		SpringApplication.run(StudentenServiceApplication.class, args);
	}
}
