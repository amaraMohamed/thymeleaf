package de.mohamed.amara.general.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class LoginModel implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private Boolean enabled;
	private Boolean isAccountNonExpired;
	private Boolean isAccountNonLocked;
	private Boolean isCredentialsNonExpired;
	
	@Builder.Default
	Set<String> mRoles = new HashSet<String>();
	
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<String> roles = getMRoles();
		
		if(roles == null) {
			return Collections.emptyList();
		}
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (String role: roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return isAccountNonExpired();
	}
	@Override
	public boolean isAccountNonLocked() {
		return isAccountNonLocked();
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return isCredentialsNonExpired();
	}
	@Override
	public boolean isEnabled() {
		return getEnabled();
	}

}
