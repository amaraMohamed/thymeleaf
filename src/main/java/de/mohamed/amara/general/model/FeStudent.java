package de.mohamed.amara.general.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(of = { "uuid" })
public class FeStudent {

	private String uuid;

	private String name;

	private String vorname;

	private Date geburtsdatum;

	private String matrikelNr;

	private String email;
	
	private FeStudiengang studiengang;

}
