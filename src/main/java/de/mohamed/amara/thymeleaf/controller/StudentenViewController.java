package de.mohamed.amara.thymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import de.mohamed.amara.general.model.LoginModel;
import de.mohamed.amara.security.service.WhsStudentenUserDetailsService;

@Controller
public class StudentenViewController {
	
	@Autowired
	WhsStudentenUserDetailsService mUserDetailsService;

	@RequestMapping("")
	public String getStudentenView(Model pModel) {
		LoginModel lLoginModel = new LoginModel();
		pModel.addAttribute("loginmodel", lLoginModel);
		return "studenten";
	}
	
	@RequestMapping("/login-error")
	public String loginError(Model pModel) {
		LoginModel lLoginModel = new LoginModel();
		pModel.addAttribute("loginmodel", lLoginModel);
		pModel.addAttribute("fehlerMeldung", true);
		return "studenten";
	}
	
	@RequestMapping("/view")
	public String getDashboardView() {
		return "dashboard";
	}
	
}
