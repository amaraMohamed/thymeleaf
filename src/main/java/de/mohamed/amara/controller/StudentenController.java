package de.mohamed.amara.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.mohamed.amara.general.model.FeStudent;
import de.mohamed.amara.service.StudentenService;

@RestController
@RequestMapping("/rest")
public class StudentenController {
	
	@Autowired
	StudentenService mStudentenService;

	@GetMapping("")
	public List<FeStudent> getAll() {
		return mStudentenService.getAll();
	}
	
	@GetMapping("/{uuid}")
	public FeStudent get(@PathVariable String uuid) {
		return mStudentenService.get(uuid);
	}
	
	@PostMapping("")
	public ResponseEntity<FeStudent> save(@RequestBody FeStudent pFeStudent) {
		return mStudentenService.save(pFeStudent);
	}
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping("/{uuid}")
	public ResponseEntity delete(@PathVariable String uuid) {
		return mStudentenService.delete(uuid);
	}
	
}
