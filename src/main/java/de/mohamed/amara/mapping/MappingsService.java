package de.mohamed.amara.mapping;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import de.mohamed.amara.entity.Student;
import de.mohamed.amara.general.model.LoginModel;

@Service
public class MappingsService {
	
	public LoginModel StudentToLoginModel(Student pStudent) {
		Set<String> lRoles = new HashSet<String>();
		lRoles.add("Student");
		return LoginModel.builder()
				.enabled(pStudent.getEnabled())
				.isAccountNonExpired(pStudent.getIsAccountNonExpired())
				.isAccountNonLocked(pStudent.getIsAccountNonLocked())
				.isCredentialsNonExpired(pStudent.getIsCredentialsNonExpired())
				.mRoles(lRoles)
				.build();
	}

}
